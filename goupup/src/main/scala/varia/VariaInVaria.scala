// longest substring without digit and with max one upper letter

object VariaInVaria {
  val zero = ("", "", false)
  def find(xs: String) = {
    val tuple = xs.foldLeft(zero)(check)
    if(tuple._1.length > tuple._2.length) tuple._1 else tuple._2
  }
  def check(acc: (String, String, Boolean), ch: Char) = {
    val(res, longest, isUpperFlag) = acc
    ch match {
      case ch if ch.isDigit => 
        if(res.length > longest.length) ("", res, false)
        else ("", longest, false)
      case ch if ch.isUpper =>
        if(isUpperFlag == false) (res + ch, longest, true)
        else {
          val starter = res.dropWhile(!_.isUpper).tail
          (starter, longest, false)
        }
      case _ => (res + ch, longest, isUpperFlag)
    }


  }


  //   if(ch.isDigit) {
  //     if(res.length > longest.length) ("", res)
  //     else ("", longest)
  //   }
  //   else (res + ch, longest)
  // }




}
