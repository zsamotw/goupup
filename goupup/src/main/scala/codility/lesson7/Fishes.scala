package codility.lesson7

// 1 ->
// 0 <-
object Fishes {
  def solution(fishes: List[(Int, Int)]): Int ={
    def loop(fishes: List[(Int,Int)]) = {

      require(fishes.forall(el =>el._1 > 0 && (el._2 == 0 || el._2 ==1)))

      def move(acc: List[(Int, Int)], el: (Int, Int)) = {
        val head = acc.head
        if(head._1 > el._1 && head._2 == 1 && el._2 == 0) acc
        else if (head._1 < el._1 && head._2 == 1 && el._2 == 0) el :: acc.tail
        else if (head._1 == el._1 && head._2 == 1 && el._2 == 0) acc.head :: el :: acc.tail
        else el :: acc
      }

      fishes match {
        case Nil => 0
        case fist :: rest =>
          def countFishes(xs: List[(Int, Int)], count: Int): List[(Int, Int)] = {
            if(count == 0) xs
            else {
              val resInReverseOrder = xs.tail.foldLeft(List(xs.head))(move)
              countFishes(resInReverseOrder.reverse, count - 1)
            }
          }
          val resInReverseOrder = countFishes(fishes, fishes.length)
          resInReverseOrder.length
      }
    }

    loop(fishes)
  }
}
