package codility.lesson7

object Bracket {

  def solution(str: String): Boolean = {
    def loop(balance1: Int, balance2: Int, balance3: Int, str: String): Boolean =  str match {
      case "" => if(balance1 == 0 && balance2 == 0 && balance3 == 0) true else false
      case s: String =>
        val head = s.head
        val tail = s.tail
        if((balance1 == 0 && head == ')') || (balance2 == 0 && head == ']') || (balance3 == 0 && head == '}')) false
        else if(balance1 > 0 && head == ')') loop(balance1 - 1, balance2, balance3, tail)
        else if(head == '(') loop(balance1 + 1, balance2, balance3, tail)
        else if(balance2 > 0 && head == ']') loop(balance1 , balance2 - 1, balance3, tail)
        else if(head == '[') loop(balance1 , balance2 + 1, balance3, tail)
        else if(balance3 > 0 && head == '}') loop(balance1 , balance2 , balance3 - 1, tail)
        else if(head == '{') loop(balance1 , balance2 , balance3 + 1, tail)
        else loop(balance1, balance2, balance3, tail)
    }

    loop(0,0,0,str)
  }
}
