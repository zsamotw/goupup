package codility.lesson7

object Nesting {

  def solution(str: String): Boolean = {
    def loop(balance: Int, str: String): Boolean =  str match {
      case "" => if(balance == 0) true else false
      case s: String =>
        val head = s.head
        val tail = s.tail
        if(balance == 0 && head == ')') false
        else if(balance > 0 && head == ')') loop(balance - 1, tail)
        else if(head == '(') loop(balance + 1, tail)
        else loop(balance, tail)
    }

    loop(0, str)
  }
}
