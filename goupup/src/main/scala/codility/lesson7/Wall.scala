package codility.lesson7

object Wall {
  def solution(xs: List[Int]): Int = {
    def loop(xs: List[Int]): Int = {
      def processor(acc: (Set[Int], Int,  Boolean, Int), el: Int) = {
        acc match {
          case (set, last, false, add) => (set + el, el, if(last > el) true else false, add)
          case (set, last, true, add) =>
            val isMinus = last > el
            if(isMinus) (set + el, el, true, add)
            else (set + el, el, false, add + 1)
        }
      }

      xs match {
        case Nil => 0
        case first :: rest =>
          val tuple = rest.foldLeft((Set.empty[Int], first, false, 0))(processor)
          tuple._1.size + tuple._4
      }
    }

    loop(xs)
  }
}
