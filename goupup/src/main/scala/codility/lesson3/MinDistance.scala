package codility.lesson3

object MinDistance {
  def solution(xs: List[Int]): Int = {
    val sum = xs.sum
    def loop(before: Int, min: Int, ys: List[Int]): Int = ys match {
      case Nil => min
      case y :: ys1 =>
        val diff = math.abs(sum - (before + y) - (before + y))
        loop(before + y, if(diff < min) diff else min, ys1)
    }

    loop(0, sum, xs)
  }
}
