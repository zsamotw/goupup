package codility.lesson3

object FrogJmp {
  def solution(from: Int, to: Int, jump: Int): Int = {
    val diff = to - from
    val ratio = diff / jump
    if (diff % jump == 0) ratio else ratio + 1
  }
}
