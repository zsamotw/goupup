package codility.lesson3

object PermMissingElem {
  def solution(xs: List[Int]): Option[Int] = {
    val sortedXs = xs.sorted

    def loop(ys: List[Int], last: Int, res: Option[Int]): Option[Int] = ys match {
      case Nil => res
      case y :: yy =>
        if (y - last != 1) {
          loop(yy, y, Some(y - 1))
        }
        else {
          loop(yy, y, res)
        }
    }

    loop(sortedXs, 0, None)
  }
}
