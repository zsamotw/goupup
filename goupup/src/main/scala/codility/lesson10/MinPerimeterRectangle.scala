
package codility.lesson10

import scala.math.sqrt

object MinPerimeterRectangle {
  def solutions(n: Int): Int = {
    (for {
      x <- 1 to (sqrt(n).toInt + 1)
      y <- 1 to (sqrt(n).toInt + 1)
      if n % x == 0 && n % y == 0 && x * y == n
    } yield {
      2 * (x + y)
    }).min
  }
}
