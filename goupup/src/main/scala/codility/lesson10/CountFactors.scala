
package codility.lesson10

import scala.math.sqrt

object CountFactors {
  def solutions(x: Int): Int = {
    def loop(x: Int, div: Int, result: Int): Int = {
      if(div*div <= x) {
        if(x % div == 0) loop(x, div + 1, if(div*div == x) result + 1 else result + 2) else loop(x, div + 1, result)
      }
      else {
        result + 2
      }
    }

    loop(x, 2, 0)
  }
}
