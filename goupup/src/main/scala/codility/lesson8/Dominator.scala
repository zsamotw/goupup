
package codility.lesson8

object Dominator {
  def solution(xs: List[Int]): Option[(Int, List[Int])] = {
    def loop(xs: List[Int], map: Map[Int, List[Int]], index: Int, length: Int): Option[(Int, List[Int])] = xs match {
      case Nil =>
        if(map isEmpty) None
        else {
          val dominator = map.toSeq.sortWith(_._2.length > _._2.length).head
          if(dominator._2.length > (index) / 2) Some(dominator._1, dominator._2) else None
        }
      case h :: t =>
        val list = index :: map.getOrElse(h, List.empty[Int])
        loop(t, map + (h -> list), index + 1, length)
    }

    loop(xs, Map.empty[Int, List[Int]], 0, xs.length)
  }
}
