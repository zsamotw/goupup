package codility.lesson8

object EquiLeaders {
  def solution(xs: List[Int]): Int = {
    def candidates(ys: List[Int]): Seq[(Int, Int)] = ys
      .groupBy(i => i)
      .mapValues(l => l.length)
      .toSeq
      .sortBy(_._2)
      .reverse

    def loop(xs: List[Int], candidate: Int, splintIndex: Int, howMany: Int): Int = {
      if (splintIndex == xs.length - 1) {
        howMany
      }
      else {
        val candidateA = candidates(xs.take(splintIndex)).head._1
        val candidateB = candidates(xs.drop(splintIndex)).head._1
        if (candidateA == candidateB && candidateA == candidate) {
          loop(xs, candidate, splintIndex + 1, howMany + 1)
        }
        else loop(xs, candidate, splintIndex + 1, howMany)
      }
    }
    loop(xs, candidates(xs).head._1, 1, 0)
  }
}
