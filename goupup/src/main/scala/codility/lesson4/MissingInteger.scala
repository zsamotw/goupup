package codility.lesson4

object MissingInteger {
  def solution(xs: List[Int]): Int = xs match {
    case Nil => 1
    case y :: ys =>
      val minMax = xs.foldLeft((y, y, Set.empty[Int])) { (acc, el) =>
        val (min, max, set) = acc
        val minN = if (el < min) el else min
        val maxN = if (el > max) el else max
        (minN, maxN, set + el)
      }
      minMax match {
        case (min, _, _) if min > 1 => 1
        case (_, max, _) if max < 1 => 1
        case (min, max, set) =>
          def findMin(x: Int): Int = {
            if(x == max) max + 1
            else if(set contains x) findMin(x + 1)
            else x
          }
          findMin(min + 1)
      }
  }
}
