package codility.lesson4

object PermCheck {
  def solution(xs: List[Int]): Boolean = {
    val sortedXs = xs.sorted
    def loop(ys: List[Int]): Boolean = ys match {
      case Nil => false
      case x :: y :: Nil => if(y - x == 1) true else false
      case y :: yy =>
        if(yy.head - y == 1) loop(yy)
        else false
    }
    loop(sortedXs)
  }
}
