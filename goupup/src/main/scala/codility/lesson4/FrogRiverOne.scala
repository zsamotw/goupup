package codility.lesson4

object FrogRiverOne {
  def solution(n: Int, leaves: List[Int]): Option[Int] =  {
    def loop(n: Int, leaves: List[Int], seconds: Int, second: Option[Int]): Option[Int]= leaves match {
      case Nil => second
      case y :: ys =>
        if(y == n) loop(n, ys, seconds + 1, Some(seconds))
        else loop(n, ys, seconds + 1, second)
    }

    loop(n, leaves, 0, None)
  }
}