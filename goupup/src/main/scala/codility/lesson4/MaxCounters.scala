package codility.lesson4

object MaxCounters {
  def solution(n: Int, operations: List[Int]): List[Int] = {
    def loop(acc: List[Int], el: Int): List[Int] = {
      if (el <= n && el >= 1) {
        for ((x, index) <- acc.zipWithIndex) yield {
          if (index == el - 1) x + 1
          else x
        }
      }
      else {
        val max = acc.max
        acc.map(_ => max)
      }
    }

    val counters = List.fill(n)(0)
    operations.foldLeft(counters)(loop)
  }
}
