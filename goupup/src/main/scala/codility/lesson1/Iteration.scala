package codility.lesson1

object BinnaryGap {
  def fromIntToString(in: Int): String =
    if(in == 0) 0.toString
    else if (in == 1) 1.toString
    else fromIntToString(in / 2) + (in % 2).toString

  def computeGap(binnary: String): Int = (binnary.foldLeft((0,0, false))(loop))._2

  def loop(result: (Int, Int, Boolean), value: Char): (Int, Int, Boolean) = {
    val(current, max, is1) = result
    if (value == '0') {
      if(is1) (current + 1, max, true)
      else (0, max, false)
    }
    else if (value == '1') {
      if(current > max) (0, current, true)
      else (0, max, true)
    }
    else throw new Exception("Wrong input")
  }

  def solution(in: Int): Int = computeGap(fromIntToString(in))
}
