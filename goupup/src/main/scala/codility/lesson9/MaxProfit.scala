
package codility.lesson9

object MaxProfit {
  def loop(acc: (Int, Int), el: Int): (Int, Int) = {
    val(min, max) = acc
    if(el < max) (if(min < el) min else el, el)
    else  (if(min < el) min  else el, if(max > el) max else el)
  }
  def solution(xs: List[Int]): Int = {
    val(min, max) = xs.tail.foldLeft(xs.head, xs.head)(loop)
    max - min
  }
}
