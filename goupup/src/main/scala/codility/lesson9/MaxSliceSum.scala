
package codility.lesson9

object MaxSliceSum {
  def loop(acc: (Int, Int), el: Int): (Int, Int) = {
    val maxEnd = acc._1 + el
    val maxSlice = scala.math.max(acc._2, maxEnd)
    (maxEnd, maxSlice)
  }

  def solution(xs: List[Int]): Int = xs match {
    case Nil => 0
    case _ =>
      val (_, res) = xs.foldLeft(0, 0)(loop)
      res
  }
}
