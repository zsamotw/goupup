package codility.lesson6

object NumberOfDiscIntersection {
  def solution(discs: List[Int]): Int = discs match {
    case Nil => 0
    case x :: Nil => 0
    case _ =>
      def loop(xs: List[(Int, Int)]): Int = xs match {
        case Nil => 0
        case y :: ys =>
          def countIntercections(acc: Int, el: (Int, Int)): Int = {
            val(radiusEl, centerEl) = el
            val(radiusY, centerY) = y
            if((centerY - radiusY) <= (centerEl - radiusEl)  && (centerEl - radiusEl) <= (centerY + radiusY)
                 || (centerY + radiusY) >= (centerEl + radiusEl) && (centerY - radiusY) <= (centerEl + radiusEl)) acc + 1
            else acc
          }
          ys.foldLeft(0)(countIntercections) + loop(ys)
      }

      val sortedDiscs = discs.zipWithIndex.sortWith((el1, el2) => el1._1 > el2._1)
      loop(sortedDiscs)
  }
}
