package codility.lesson6

object MaxProductOfThree {
  def solution(xs: List[Int]): Int = {
    def loop(acc: (Int, Int, Int), el: Int): (Int, Int, Int) = {
      val(prevprev, prev, product) = acc
      val actualProduct = prevprev * prev * el
      if(actualProduct > product) (prev, el, actualProduct)
      else (prev, el, product)
    }

    val xsSorted = xs.sorted
    xsSorted match {
      case Nil => 0
      case x :: Nil => 0
      case x :: y :: Nil => 0
      case x :: y :: z :: zz => zz.foldLeft(y, z, x * y * z)(loop)._3
    }
  }
}
