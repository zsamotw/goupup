package codility.lesson6

object Distinct {
  def solution(xs: List[Int]): Int = {
    def loop(acc: (Int, Int), el: Int): (Int, Int) = {
      val(prev, distintcts) = acc
      if(prev == el) (el, distintcts)
      else (el, distintcts + 1)
    }

    val xsSorted = xs.sorted
    xsSorted match {
      case Nil => 0
      case y :: ys => ys.foldLeft(y, 0)(loop)._2 + 1
    }
  }
}
