package codility.lesson6

object Triangle {
  def solution(xs: List[Int]): Boolean  = {
    def loop(xs: List[Int]): Boolean = {
      {
        for {
          x <- xs
          y <- xs.span(_ <= x)._2
        } yield {
          xs.span(_ <= y)._2.exists{z => (x + y > z && y + z > x && z + x > y)
          }
        }
      }.exists(el => el == true)
    }

    val xsSorted = xs.sorted
    xsSorted match {
      case Nil => false
      case x :: Nil => false
      case x :: y :: Nil => false
      case x :: y :: z :: zz => loop(xsSorted) 
    }
  }
}
