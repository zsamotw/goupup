package codility.lesson5

object PassingCars {
  def solution(cars: List[Int]): Int  = {
    def loop(acc: (Int, Int), car: Int): (Int, Int) = {
      val(west, passing) = acc
      if(car == 0) (west + 1, passing)
      else if(car == 1) (west, passing + west)
      else (west, passing)
    }
    cars match {
      case Nil => 0
      case _ => cars.foldLeft((0,0))(loop)._2
    }
  }
}
