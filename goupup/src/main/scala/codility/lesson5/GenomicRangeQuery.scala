package codility.lesson5

object GenomicRangeQuery {
  def solution(genom: String, p: Stream[Int], q: Stream[Int]): List[Int] = {
    def genomToValues(genom: String): List[Int] = genom match {
      case "" => Nil
      case chars =>
        val ch = chars.head
        if(ch == 'A') 1 :: genomToValues(chars.tail)
        else if(ch == 'B') 2 :: genomToValues(chars.tail)
        else if(ch == 'G') 3 :: genomToValues(chars.tail)
        else if(ch == 'T') 4 :: genomToValues(chars.tail)
        else Nil
    }

    val genomValues = genomToValues(genom)

    def loop(factors: List[Int], el: (Int, Int)): List[Int] = {
      val partGenom = genomValues.slice(el._1, el._2)
      partGenom match {
        case Nil => factors
        case _ => factors ::: List(partGenom.min)
      }
    }

    (p zip q).foldLeft(List.empty[Int])(loop)
  }
}
