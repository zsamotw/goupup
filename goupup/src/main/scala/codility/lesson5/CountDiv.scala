package codility.lesson5

object CountDiv {
  def solution(a: Int, b: Int, c: Int): Int  = {
    def loop(count: Int, el: Int): Int = {
      if(el % c == 0) count + 1 else count
    }
    if(a > b) 0
    else if(a < 0 || b < 0 || c < 1) 0
    else (a to b).foldLeft(0)(loop)
  }
}
