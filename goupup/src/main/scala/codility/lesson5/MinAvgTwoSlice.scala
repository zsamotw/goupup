
package codility.lesson5

object MinAvgTwoSlice {
  def solution(xs: List[Int]): Int = {
    def loop(acc: (Int, Int, Option[Double], Int), el: Int) = {
      val(last, lastIndex, minAvgOpt, minStartIndex) = acc
      val avg = (last + el).toDouble / 2
      minAvgOpt match {
        case None => (el, lastIndex + 1, Some(avg), minStartIndex)
        case Some(minAvg) =>
          if(avg >= minAvg) (el, lastIndex + 1, minAvgOpt, minStartIndex)
          else  (el, lastIndex + 1, Some(avg), lastIndex)

      }
    }
    xs match {
      case Nil => 0
      case x :: Nil => 0
      case y :: ys => ys.foldLeft((y, 0, Option.empty[Double], 0))(loop)._4
    }
  }
}
