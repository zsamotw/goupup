package codility.lesson2

object CyclicRotation {
  def solution[T](xs: List[T], k: Int): List[T] = xs match {
    case Nil => Nil
    case ys =>
      if(k == 0) ys
      else solution(ys.last :: ys.init, k - 1)
  }
}
