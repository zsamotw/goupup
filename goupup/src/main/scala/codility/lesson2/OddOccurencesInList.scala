package codility.lesson2

object OddOccurencesInList {

  def loop(ss: Set[Int], elem: Int) = {
    if (ss contains elem) ss - elem
    else ss + elem
  }

  def solution(xs: List[Int]): Int = xs.foldLeft(Set.empty[Int])(loop).head
}
