package codility.lesson2

import org.scalatest._
import Matchers._

class CyclicRotationTest extends FlatSpec {
  "List(1,2,3) after rotate with 2" should "be List(2,3,1)" in {
    CyclicRotation.solution(List(1,2,3), 2) shouldBe List(2,3,1)
  }
  "List(3, 8, 9, 7, 6) after rotate with 3" should "be List(9, 7, 6, 3, 8)" in {
    CyclicRotation.solution(List(3, 8, 9, 7, 6), 3) shouldBe List(9, 7, 6, 3, 8)
  }
  "List(0, 0, 0) after rotate with 1" should "be List(0, 0, 0)" in {
    CyclicRotation.solution(List(0, 0, 0), 1) shouldBe List(0, 0, 0)
  }
  "List(1, 2, 3, 4) after rotate with 4" should "be List(1, 2, 3, 4)" in {
    CyclicRotation.solution(List(1, 2, 3, 4), 4) shouldBe List(1, 2, 3, 4)
  }
}
