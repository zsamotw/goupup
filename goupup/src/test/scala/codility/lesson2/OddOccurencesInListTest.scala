package codility.lesson2

import org.scalatest._
import Matchers._

class OddOccurencesInListTest extends FlatSpec {

  "List(1,2,1)" should "gives result as 2" in {
    OddOccurencesInList.solution(List(1,2,1)) shouldBe 2
  }

  "List(1,2,1,3,4,4,3)" should "gives result as 2" in {
    OddOccurencesInList.solution(List(1,2,1,3,4,4,3)) shouldBe 2
  }

  "List(3,1,2,1,2)" should "gives result as 3" in {
    OddOccurencesInList.solution(List(3,1,2,1,2)) shouldBe 3
  }
}
