
package codility.lesson9

import org.scalatest._
import Matchers._

class MaxSliceSumTest extends FlatSpec {
  "MaxSliceSum in List()" should "be 0" in {
    MaxSliceSum.solution(List()) shouldBe 0
  }

  "MaxSliceSum in List(3,2,-6,4,0)" should "be 5" in {
    MaxSliceSum.solution(List(3,2,-6,4,0)) shouldBe 5
  }

  "MaxSliceSum in List(1,2,3,4)" should "be 10" in {
    MaxSliceSum.solution(List(1,2,3,4)) shouldBe 10
  }
}
