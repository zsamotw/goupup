
package codility.lesson9

import org.scalatest._
import Matchers._

class MaxProfitTest extends FlatSpec {
  "MaxProfit in List(23171, 21011, 21123, 21366, 21013, 21367)" should "be 356" in {
    MaxProfit.solution(List(23171, 21011, 21123, 21366, 21013, 21367)) shouldBe 356
  }

  "MaxProfit in List(2,3,2,1,4)" should "be 3" in {
    MaxProfit.solution(List(2,3,2,1,4)) shouldBe 3
  }

  "MaxProfit in List(4,3,2,1)" should "be 0" in {
    MaxProfit.solution(List(4,3,2,1)) shouldBe 0
  }

  "MaxProfit in List(5,4,3,2,1,5)" should "be 4" in {
    MaxProfit.solution(List(5,4,3,2,1,5)) shouldBe 4
  }
}
