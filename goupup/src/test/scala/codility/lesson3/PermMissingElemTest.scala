package codility.lesson3

import org.scalatest._
import Matchers._

 class PermMissingElemTest extends FlatSpec {
   "List(1, 2, 3, 4, 6) " should "" in {
     PermMissingElem.solution(List(1, 2, 3, 4, 6)) shouldBe Some(5)
   }
 }
