package codility.lesson3

import org.scalatest._
import Matchers._

class MinDistanceTest extends FlatSpec {
  "In List(3,1,2,4,3) min distance" should "be 1" in {
    MinDistance.solution(List(3,1,2,4,3)) shouldBe 1
  }

  "In List(1,1,1,1) min distance" should "be 0" in {
    MinDistance.solution(List(1,1,1,1)) shouldBe 0
  }

  "In Nil min distance" should "be 0" in {
    MinDistance.solution(Nil) shouldBe 0
  }
}
