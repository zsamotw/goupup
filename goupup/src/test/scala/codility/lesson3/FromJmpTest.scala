package codility.lesson3

import org.scalatest._
import Matchers._

class FrogJmpTest extends FlatSpec {

  "from 10 to 85 with jump 30" should "be 3" in {
    FrogJmp.solution(10, 85, 30) shouldBe 3
  }

  "from 0 to 100 with jump 10" should "be 10" in {
    FrogJmp.solution(0, 100, 10) shouldBe 10
  }
}
