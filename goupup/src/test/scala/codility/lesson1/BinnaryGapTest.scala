package codility.lesson1

import org.scalatest._
import Matchers._

class BinnaryGapTest extends FlatSpec {

  "9 as String" should "be 1001" in {
    BinnaryGap.fromIntToString(9) shouldBe "1001"
  }

  "529 as String" should "be 1000010001" in {
    BinnaryGap.fromIntToString(529) shouldBe "1000010001"
  }

  "20 as String" should "be 10100" in {
    BinnaryGap.fromIntToString(20) shouldBe "10100"
  }

  "15 as String" should "be 1111" in {
    BinnaryGap.fromIntToString(15) shouldBe "1111"
  }

  "32 as String" should "be 100000" in {
    BinnaryGap.fromIntToString(32) shouldBe "100000"
  }

  "Gap in 1001" should "be 2" in {
    val in = "1001"
    BinnaryGap.computeGap(in) shouldBe 2
  }

  "Gap in 10010000" should "be 2" in {
    val in = "10010000"
    BinnaryGap.computeGap(in) shouldBe 2
  }

  "Gap in 100100001" should "be 4" in {
    val in = "100100001"
    BinnaryGap.computeGap(in) shouldBe 4
  }

  "Gap in 000000000100100001" should "be 4" in {
    val in = "000000000100100001"
    BinnaryGap.computeGap(in) shouldBe 4
  }

  "Gap in 0001" should "be 0" in {
    val in = "0001"
    BinnaryGap.computeGap(in) shouldBe 0
  }
}
