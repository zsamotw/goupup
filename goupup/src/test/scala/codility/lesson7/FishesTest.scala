
package codility.lesson7

import org.scalatest._
import Matchers._

// 1 ->
// 0 <-
class FishesTest extends FlatSpec {
  "Fishes List((4,1), (4,1))" should "be 2" in {
    Fishes.solution(List((4,1), (4,1))) shouldBe 2
  }

  "Fishes List((4,1), (3,0))" should "be 1" in {
    Fishes.solution(List((4,1), (3,0))) shouldBe 1
  }

  "Fishes List((4,1), (3,0), (5,0), (1,1))" should "be 2" in {
    Fishes.solution(List((4,1), (3,0), (5,0), (1,1))) shouldBe 2
  }

  "Fishes List((10,1),(1,1),(4,1), (3,0), (5,0), (1,1))" should "be 2" in {
    Fishes.solution(List((10,1),(1,1),(4,1), (3,0), (5,0), (1,1))) shouldBe 2
  }
}
