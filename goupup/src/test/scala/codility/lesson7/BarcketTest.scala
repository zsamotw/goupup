package codility.lesson7

import org.scalatest._
import Matchers._

class BracketTest extends FlatSpec {
  "In empty string" should "balance of brackets is true" in {
    Bracket.solution("") shouldBe true
  }

  "In string \"()[]{}\"" should "balance of brackets is true" in {
    Bracket.solution("()[]{}") shouldBe true
  }

  "In string \"([])[{()}]{()[()]}\"" should "balance of brackets is true" in {
    Bracket.solution("([])[{()}]{()[()]}") shouldBe true
  }

  "In string \"()[)]{}\"" should "balance of brackets is false" in {
    Bracket.solution("()[)]{}") shouldBe false
  }
  "In string \")\"" should "balance of brackets is false" in {
    Bracket.solution(")") shouldBe false
  }

  "In string \"]\"" should "balance of brackets is false" in {
    Bracket.solution("]") shouldBe false
  }

  "In string \"}\"" should "balance of brackets is false" in {
    Bracket.solution("}") shouldBe false
  }

  "In string \"()({}\"" should "balance of brackets is false" in {
    Bracket.solution("()({}") shouldBe false
  }

  "In string \"()[{}\"" should "balance of brackets is false" in {
    Bracket.solution("()[{}") shouldBe false
  }

  "In string \"(){{}\"" should "balance of brackets is false" in {
    Bracket.solution("(){{}") shouldBe false
  }
}
