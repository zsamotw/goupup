package codility.lesson7

import org.scalatest._
import Matchers._

class WallTest extends FlatSpec {
    "In wall as List.empty" should "number of blocks is 0" in {
      Wall.solution(List.empty[Int]) shouldBe 0
    }

  "In wall as List(1,2,3,2,1)" should "number of blocks is 3" in {
    Wall.solution(List(1,2,3,2,1)) shouldBe 3
  }

  "In wall as List(1,2,3,2,1,3,4,4)" should "number of blocks is 5" in {
    Wall.solution(List(1,2,3,2,1,3,4,4)) shouldBe 5
  }

  "In wall as List(8,8,5,7,9,8,7,4,8)" should "number of blocks is 7" in {
    Wall.solution(List(8,8,5,7,9,8,7,4,8)) shouldBe 7
  }
}
