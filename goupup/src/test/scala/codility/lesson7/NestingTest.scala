package codility.lesson7

import org.scalatest._
import Matchers._

class NestingTest extends FlatSpec {
  "In empty string" should "balance is true" in {
    Nesting.solution("") shouldBe true
  }

  "In string \"()()\"" should "balance is true" in {
    Nesting.solution("()()") shouldBe true
  }

  "In string \"())()\"" should "balance is false" in {
    Nesting.solution("())()") shouldBe false
  }

  "In string \")()\"" should "balance is true" in {
    Nesting.solution(")()") shouldBe false
  }

  "In string \"(()())(()((())))\"" should "balance is true" in {
    Nesting.solution("(()())(()((())))") shouldBe true
  }
}
