package codility.lesson6

import org.scalatest._
import Matchers._

class MaxProductOfThreeTest extends FlatSpec {
  "Max product in empty list" should "be 0" in {
    MaxProductOfThree.solution(Nil) shouldBe 0
  }

  "Max product in List(1) list" should "be 0" in {
    MaxProductOfThree.solution(List(1)) shouldBe 0
  }

  "Max product in List(1, 2) list" should "be 0" in {
    MaxProductOfThree.solution(List(1, 2)) shouldBe 0
  }

  "Max product in List(1, 2, 3) list" should "be 6" in {
    MaxProductOfThree.solution(List(1, 2, 3)) shouldBe 6
  }

  "Max product in List(1, 2, 3, 1, 10, 4, 3, 10, 2,3) list" should "be 400" in {
    MaxProductOfThree.solution(List(1, 2, 3, 1, 10, 4, 3, 10, 2,3)) shouldBe 400
  }
}
