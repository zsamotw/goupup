package codility.lesson6

import org.scalatest._
import Matchers._

class DistinctTest extends FlatSpec {
  "Distincts in empty list" should "be 0" in {
    Distinct.solution(Nil) shouldBe 0
  }

  "Distincts in List(1,2,3,4)" should "be 4" in {
    Distinct.solution(List(1,2,3,4)) shouldBe 4
  }

  "Distincts in List(1,2,1,3,4,1,2)" should "be 4" in {
    Distinct.solution(List(1,2,1,3,4,1,2)) shouldBe 4
  }

  "Distincts in List(1,1,2,1,3,4,1,2)" should "be 4" in {
    Distinct.solution(List(1,1,2,1,3,4,1,2)) shouldBe 4
  }

  "Distincts in List(1)" should "be 1" in {
    Distinct.solution(List(1)) shouldBe 1
  }
}
