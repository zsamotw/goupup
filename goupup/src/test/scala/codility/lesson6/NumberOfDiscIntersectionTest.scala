package codility.lesson6

import org.scalatest._
import Matchers._

class NumberOfDiscIntersectionTest extends FlatSpec {
  "Discs intersections in empty list" should "be 0" in {
    NumberOfDiscIntersection.solution(List.empty) shouldBe 0
  }

  "Discs intersections in List(1)" should "be 0" in {
    NumberOfDiscIntersection.solution(List(0)) shouldBe 0
  }

  "Discs intersections in List(1,2,3,4)" should "be 6" in {
    NumberOfDiscIntersection.solution(List(1,2,3,4)) shouldBe 6
  }

  "Discs intersections in List(0,0,0,0)" should "be 0" in {
    NumberOfDiscIntersection.solution(List(0,0,0,0)) shouldBe 0
  }

  "Discs intersections in List(0, 1, 5, 0)" should "be 4" in {
    NumberOfDiscIntersection.solution(List(0,1,5,0)) shouldBe 4
  }

  "Discs intersections in List(1,5,2,1,4,0)" should "be 11" in {
    NumberOfDiscIntersection.solution(List(1,5,2,1,4,0)) shouldBe 11
  }
}
