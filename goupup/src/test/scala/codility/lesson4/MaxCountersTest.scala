package codility.lesson4

import org.scalatest._
import Matchers._

class MaxCountersTest extends FlatSpec {
  "For operations as List(3,4,4,6,1,4,4)" should "result be List(3,2,2,4,2)" in {
    MaxCounters.solution(5, List(3,4,4,6,1,4,4)) shouldBe List(3,2,2,4,2)
  }

  "For operations as List(1,1) and n == 2" should "result be List(2,0)" in {
    MaxCounters.solution(2, List(1,1)) shouldBe List(2,0)
  }
}
