package codility.lesson4

import org.scalatest._
import Matchers._

class FrogRiverOneTest extends FlatSpec {
  "Frog in Nil" should "be None" in {
    FrogRiverOne.solution(3, Nil) shouldBe None
  }

  "Frog in List(1,3,2) and 2" should "be Some(2)" in {
    FrogRiverOne.solution(2, List(1,3,2)) shouldBe Some(2)
  }

  "Frog in List(4,5,6,7) and 4" should "be Some(0)" in {
    FrogRiverOne.solution(4, List(4,5,6,7)) shouldBe Some(0)
  }
}
