package codility.lesson4

import org.scalatest._
import Matchers._

class MissingIntegerTest extends FlatSpec {
  "In Nil min integer" should "be 1" in {
    MissingInteger.solution(Nil) shouldBe 1
  }

  "In List(2,3,4,5,6,7) min integer " should "be 1" in {
    MissingInteger.solution(List(2,3,4,5,6,7)) shouldBe 1
  }

  "In List(1,2,3,4,5,6,7) min integer " should "be 8" in {
    MissingInteger.solution(List(1,2,3,4,5,6,7)) shouldBe 8
  }

  "In List(-2,-1) min integer " should "be 1" in {
    MissingInteger.solution(List(-2,-1)) shouldBe 1
  }

  "In List(1,2,3,5,7) min integer " should "be 4" in {
    MissingInteger.solution(List(1,2,3,5,7)) shouldBe 4
  }
}
