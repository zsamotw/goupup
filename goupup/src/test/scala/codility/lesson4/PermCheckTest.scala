package codility.lesson4

import org.scalatest._
import Matchers._

class PermCheckTest extends FlatSpec {
  "PermCheck in Nil" should "be false" in {
    PermCheck.solution(Nil) shouldBe false
  }

  "PermCheck in List(1,2,3)" should "be true" in {
    PermCheck.solution(List(1,2,3)) shouldBe true
  }

  "PermCheck in List(3,5,4,2)" should "be true" in {
    PermCheck.solution(List(3,5,4,2)) shouldBe true
  }

  "PermCheck in List(1,1,1)" should "be false" in {
    PermCheck.solution(List(1,1,1)) shouldBe false
  }

  "PermCheck in List(1,2,3,6)" should "be false" in {
    PermCheck.solution(List(1,2,3,6)) shouldBe false
  }
}
