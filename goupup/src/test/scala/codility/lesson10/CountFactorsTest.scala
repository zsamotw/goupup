
package codility.lesson10

import org.scalatest._
import Matchers._

class CountFactorsTest extends FlatSpec {

  "Factors of 2" should "be 2" in {
    CountFactors.solutions(2) shouldBe 2
  }

  "Factors of 3" should "be 2" in {
    CountFactors.solutions(3) shouldBe 2
  }

  "Factors of 4" should "be 3" in {
    CountFactors.solutions(4) shouldBe 3
  }

  "Factors of 5" should "be 2" in {
    CountFactors.solutions(5) shouldBe 2
  }

  "Factors of 6" should "be 4" in {
    CountFactors.solutions(6) shouldBe 4
  }

  "Factors of 24" should "be 8" in {
    CountFactors.solutions(24) shouldBe 8
  }
}

