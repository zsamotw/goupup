
package codility.lesson10

import org.scalatest._
import Matchers._

class MinPerimeterRectangleTest extends FlatSpec {
  "Min perimeter of 30" should "be 22" in {
    MinPerimeterRectangle.solutions(30) shouldBe 22
  }
}
