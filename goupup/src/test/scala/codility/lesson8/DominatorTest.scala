package codility.lesson8

import org.scalatest._
import Matchers._

class DominatorTest extends FlatSpec {
  "In empty List " should "be no dominator" in {
    Dominator.solution(List.empty[Int]) shouldBe None
  }

  "In List(1,2,3,4) " should "be no dominator" in {
    Dominator.solution(List(1,2,3,4)) shouldBe None
  }

  "In List(1,2,3,2,4) " should "be no dominator" in {
    Dominator.solution(List(1,2,3,2,4)) shouldBe None
  }

  "In List(1,2,3,2,4,2,2) " should "be  dominator 2" in {
    Dominator.solution(List(1,2,3,2,4,2,2)) shouldBe Some(2, List(6,5,3,1)) 
  }
}
