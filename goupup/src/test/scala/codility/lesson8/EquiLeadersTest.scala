
package codility.lesson8

import org.scalatest._
import Matchers._

class EquiLeadersTest extends FlatSpec {
  "Leaders in List(4,3,4,4,4,2)" should "be 2" in {
    EquiLeaders.solution(List(4,3,4,4,4,2)) shouldBe 2
  }
}
