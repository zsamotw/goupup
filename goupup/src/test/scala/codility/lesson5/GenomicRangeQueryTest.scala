package codility.lesson5

import org.scalatest._
import Matchers._

class GenomicRangeQueryTest extends FlatSpec {
  "For genom = \"ABBBB\" and p = (0) q = (4)" should
    "factors be List(1)" in {
      GenomicRangeQuery.solution("ABBBB", Stream(0), Stream(4)) shouldBe List(1)
  }

  "For genom = \"ABTBA\" and p = (0, 1, 3) q = (1, 3, 5)" should
  "factors be List(1,2,1)" in {
    GenomicRangeQuery.solution("ABTBA", Stream(0,1,3), Stream(1,3,5)) shouldBe List(1,2,1)
  }

  "For genom = \"ABTBA\" and p = (0, 10, 3) q = (1, 3, 5)" should
  "factors be List(1,1)" in {
    GenomicRangeQuery.solution("ABTBA", Stream(0,10,3), Stream(1,3,5)) shouldBe List(1,1)
  }

  "For genom = \"ABTBA\" and p = (0, 1, 3) q = (1, 3, 5, 1)" should
  "factors be List(1,2,1)" in {
    GenomicRangeQuery.solution("ABTBA", Stream(0,1,3), Stream(1,3,5, 1)) shouldBe List(1,2,1)
  }
}
