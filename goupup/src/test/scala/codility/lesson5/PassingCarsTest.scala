package codility.lesson5

import org.scalatest._
import Matchers._

class PassingCarsTest extends FlatSpec {
  "Empty list of cars" should " return 0 pairs of passing cars" in {
    PassingCars.solution(Nil) shouldBe 0
  }

  "Cars List(0, 0, 1)" should " return 2 pairs of passing cars" in {
    PassingCars.solution(List(0,0,1)) shouldBe 2
  }

  "Cars List(0,1,0,1,1)" should " return 5 pairs of passing cars" in {
    PassingCars.solution(List(0,1,0,1,1)) shouldBe 5
  }
}
