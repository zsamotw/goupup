package codility.lesson5

import org.scalatest._
import Matchers._

class MinAvgTwoSliceTest extends FlatSpec {
  "In List(4,2,2,5,1,5,8) min slice index" should " be 1"  in {
    MinAvgTwoSlice.solution(List(4,2,2,5,1,5,8)) shouldBe 1
  }

  "In List(4,2,0,0,1,5,8) min slice index" should " be 2"  in {
    MinAvgTwoSlice.solution(List(4,2,0,0,1,5,8)) shouldBe 2
  }

  "In List(4,2,-1,0,1,5,8) min slice index" should " be 2"  in {
    MinAvgTwoSlice.solution(List(4,2,-1,0,1,5,8)) shouldBe 2
  }

  "In Empty list min slice index" should " be 0"  in {
    MinAvgTwoSlice.solution(List.empty[Int]) shouldBe 0
  }

  "In one element list min slice index" should " be 0"  in {
    MinAvgTwoSlice.solution(List(1)) shouldBe 0
  }
}
