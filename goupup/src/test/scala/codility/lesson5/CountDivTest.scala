package codility.lesson5

import org.scalatest._
import Matchers._

class CountDivTest extends FlatSpec {
  "a = 1 , b = 10 , c = 5" should "result = 2" in {
    CountDiv.solution(1,10,5) shouldBe 2
  }
  "a = 6 , b = 11 , c = 2" should "result = 3" in {
    CountDiv.solution(6,11,2) shouldBe 3
  }
  "a = -1 , b = 11 , c = 2" should "result = 0" in {
    CountDiv.solution(-1, 11, 2) shouldBe 0
  }
  "a = 1 , b = -11 , c = 2" should "result = 0" in {
    CountDiv.solution(1, -11, 2) shouldBe 0
  }
  "a = 1 , b = 11 , c = 0" should "result = 0" in {
    CountDiv.solution(1,11,0) shouldBe 0
  }
}
