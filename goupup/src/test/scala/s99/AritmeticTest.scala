package s99.aritmetic

import org.scalatest._
import Matchers._
import Arithmetic._

class ArithmeticTest extends FlatSpec {
  val s = new S99Int(10)
  implicit def intToS99Int(x: Int) = new S99Int(x)

  "GCD of 25, 10" should "be 5" in {
    s.gcd(25,10) shouldBe 5
  }

  "GCD of 36, 63" should "be 9" in {
    s.gcd(36,63) shouldBe 9
  }

  "35" should "be coPrime of 64" in {
    35.isCoprimeTo(64) shouldBe true
  }

  "10" should "be totient with 4" in {
    10.totient shouldBe 4
  }

  "315 primes factor" should "be List(3,5,7)" in {
    315.primeFactors shouldBe List(3,5,7)
  }

  "28 golbach" should "be (5,23)" in {
    28.golbach shouldBe (5,23)
  }
}
