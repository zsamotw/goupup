package s99.trees

import org.scalatest._
import Matchers._

class TreeTest extends FlatSpec {
  /*
   * Tests  of addValue method
   */
  "End after addVAlue " should "be Node" in {
    End.addValue(1) shouldBe Node(1, End, End) 
  }
  
  "Node(1, End, End) after addValue(2)" should "be Node(1, End, Node(2, End, End))" in {
    Node(1, End, End).addValue(2) shouldBe Node(1, End, Node(2, End, End)) 
  }
  
  "Node(2, End, Node(3, End, End)) after addValue(1)" should "be Node(2, Node(1, End, End), Node(3, End, End))" in {
    Node(2, End, Node(3, End, End)).addValue(1) shouldBe Node(2, Node(1, End, End), Node(3, End, End)) 
  }
 /*
  * Tests of isSymetric method 
  */
  "End" should "be not symetric" in {
    MyTree.isSymetric(End) shouldBe false
  }
  
  "Node(1, End, End)" should "be symetric" in {
    MyTree.isSymetric(Node(1, End, End)) shouldBe true
  }
  
  "Node(1, End, Node(2, End, End))" should "be not symetric" in {
    MyTree.isSymetric(Node(1, End, Node(2, End, End))) shouldBe false
  }
  
  "Node(1, Node(3, End, End), Node(2, End, End))" should "be symetric" in {
    MyTree.isSymetric(Node(1, Node(3, End, End), Node(2, End, End))) shouldBe true
  }
  
  "Node(1, Node(3, End, End), Node(2, End, Node(4, End, End)))" should "be not symetric" in {
    MyTree.isSymetric(Node(1, Node(3, End, Node(4, End, End)), Node(2, End, End))) shouldBe false
  }
  
  /*
   * Tests of leafCount method
   */
  "End" should "has no leafs" in {
    MyTree.leafCount(End) shouldBe 0
  }

  "Node(1, End, End)" should "has 1 leaf" in {
    MyTree.leafCount(Node(1, End, End)) shouldBe 1
  }

  "Node(1, End, Node(2, End, End))" should "has 2 leafs" in {
    MyTree.leafCount(Node(1, End, Node(2, End, End))) shouldBe 2
  }
  
  "Node(1, Node(3, End, End), Node(2, End, End))" should "has 3 leafs" in {
    MyTree.leafCount(Node(1, Node(3, End, End), Node(2, End, End))) shouldBe 3
  }
  
  "Node(1, Node(3, End, Node(5, End, End), Node(2, End, End))" should "has 3 leafs" in {
    MyTree.leafCount(Node(1, Node(3, End, Node(5, End, End)), Node(2, End, End))) shouldBe 4
  }
  
  /*
   * Tests of leafList method
   */
  "End" should "has Nil" in {
    MyTree.leafList(End) shouldBe Nil
  }

  "Node(1, End, End)" should "has List(1) of leafs" in {
    MyTree.leafList(Node(1, End, End)) shouldBe List(1) 
  }

  "Node(1, End, Node(2, End, End))" should "has List(1,2) of leafs" in {
    MyTree.leafList(Node(1, End, Node(2, End, End))) shouldBe List(1,2)
  }
  
  "Node(1, Node(3, End, End), Node(2, End, End))" should "has List(1,3,2) of leafs" in {
    MyTree.leafList(Node(1, Node(3, End, End), Node(2, End, End))) shouldBe List(1,3,2)
  }
  
  "Node(1, Node(3, End, Node(5, End, End), Node(2, End, End))" should "has List(1,3,5,2) of leafs" in {
    MyTree.leafList(Node(1, Node(3, End, Node(5, End, End)), Node(2, End, End))) shouldBe List(1,3,5,2) 
  }
  
  /*
   * Tests of atLevel method
   */
  "End" should "has Nil at level 1" in {
    MyTree.atLevel(1, End) shouldBe Nil
  }
  
  "End" should "has Nil at level 2" in {
    MyTree.atLevel(2, End) shouldBe Nil
  }
  
  "Node(1, End, Node(2, End, Node(3, End, End))" should "has List(2) at level 2" in {
    MyTree.atLevel(2, Node(1, End, Node(2, End, Node(3, End, End)))) shouldBe List(2)
  }
  
  "Node(1, Node(4, Node(5, End, End), Node(6, End, End)), Node(2, End, Node(3, End, End))" should "has List(5,6,3) at level 3" in {
    MyTree.atLevel(3, Node(1, Node(4, Node(5, End, End), Node(6, End, End)), Node(2, End, Node(3, End, End)))) shouldBe List(5,6,3)
  }
}














