package s99.list

import java.util.NoSuchElementException
import org.scalatest._
import Matchers._
import Lists._

class ListTest extends FlatSpec {

  val xs = List(1,2,3,4,5)
  val xx = List(1,1,2,1,1)
  val ys = List(1,1,4,4,4,5)
  val zs = List((1,3),(2,1), (3,2), (1,1))

  "Last in xs" should "be 5" in {
    last(xs) shouldBe 5
  }

  it should "throw NoSuchElementException" in {
    assertThrows[NoSuchElementException] {
      last(Nil)
    }
  }

  "2nd in xs" should "be 3" in {
    nth(2, xs) shouldBe 3
  }


  "1st in xs" should "be 1" in {
    nth(0, xs) shouldBe 1
  }

  "length of xs" should "be 5" in {
    Lists.length(xs) shouldBe 5
  }

  "length of Empty List" should "be 0" in {
    Lists.length(Nil) shouldBe 0
  }

  "reverse of xs" should "be List(5,4,3,2,1)" in {
    reverse(xs) shouldBe List(5,4,3,2,1)
  }

  "isPalindrome xx" should "be true" in {
    assertResult(true) {
      isPalindrome(xx)
    }
  }

  "isPalindrome xs" should "be false" in {
    assertResult(false) {
      isPalindrome(xs)
    }
  }

  "Compress of ys" should "be List(1,4,5)" in {
    compress(ys) shouldBe List(1,4,5)
  }

  "Pack of ys" should "be List(List(1,1), List(4,4,4), List(5))" in {
    pack(ys) shouldBe List(List(1,1), List(4,4,4), List(5))
  }

  "Encode of ys" should "be List((1,2), (4,3), (5,1))"in {
    encode(ys) shouldBe List((1,2), (4,3), (5,1))
  }

  "EncodeDirectly of ys" should "be List((1,2), (4,3), (5,1))"in {
    encodeDirectly(ys) shouldBe List((1,2), (4,3), (5,1))
  }

  "Decode of zs" should "be List(1,1,1,2,3,3,1)"in {
    decode(zs) shouldBe List(1,1,1,2,3,3,1)
  }

  "Duplicate of List(1,2,3)" should "be List(1,1,2,2,3,3,)" in {
    duplicate(List(1,2,3)) shouldBe List(1,1,2,2,3,3)
  }

  "DuplicateN 3 of List(1,2,3)" should "be List(1,1,1,2,2,2,3,3,3)" in {
    duplicateN(List(1,2,3),3) == List(1,1,1,2,2,2,3,3,3)
  }

  "Rotate 3 of ys" should "be List(4,4,5,1,1,4)" in {
    rotate(3,ys) shouldBe List(4,4,5,1,1,4)
  }

  "Rotate -2  of ys" should "be List(4,5,1,1,4,4,)" in {
    rotate(-2, ys) shouldBe List(4,5,1,1,4,4)
  }

  "Remove 1 form ys" should "be List(1,4,4,4,5)" in {
    remove(1, ys) shouldBe (1, List(1,4,4,4,5))
  }

  "Insert At 4, 3 on List(1,2,3,5)" should "be List(1,2,3,4,5)" in {
    insertAt(4, 3,List(1,2,3,5)) shouldBe List(1,2,3,4,5)
  }

  "Range form 1 to 4" should "be List(1,2,3,4)" in {
    range(1,4) shouldBe List(1,2,3,4)
  }

  "Randomselect" should "be printed" in {
    println("list after randomSelect :" + randomSelect(3,List(1,2,3,4,5)))
    println("list after randomPermute: " + randomPermute(List(1,2,3,4)))
    println("list after combinations: " + combinations(3,List(1,2,3,4,5,6,7)))
  }
}
